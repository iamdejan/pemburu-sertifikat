# Pemburu Sertifikat

Repository ini ditujukan kepada programmer yang senang berburu sertifikat digital, entah untuk pamer, promosi, atau untuk pindah kerjaan. Semoga berguna!

## Daftar Isi

- [Petunjuk Umum](#petunjuk-umum)
- [Amazon Web Service](#amazon-web-service)
  * [Garis Besar](#garis-besar)
  * [Certified Cloud Practitioner](#certified-cloud-practitioner)
  * [Certified Cloud Developer Associate](#certified-cloud-developer-associate)
  * [Certified Solution Architect Associate](#certified-solution-architect-associate)
  * [Certified SysOps Administrator Associate](#certified-sysops-administrator-associate)
- [Google Cloud Platform](#google-cloud-platform)
  * [Garis Besar](#garis-besar-1)
  * [Cloud Digital Leader](#cloud-digital-leader)
- [Microsoft Azure](#microsoft-azure)
  * [Garis Besar](#garis-besar-2)
  * [Azure Fundamentals](#azure-fundamentals)
  * [Azure Administrator](#azure-administrator)
  * [Azure Infrastructure Solutions](#azure-infrastructure-solutions)
- [Oracle Cloud](#oracle-cloud)
  * [Garis Besar](#garis-besar-3)
  * [Fundamentals Associate](#fundamentals-associate)
- [Terraform](#terraform)
- [Kubernetes](#kubernetes)
  * [Kubernetes and Cloud Native Associate (KCNA)](#kubernetes-and-cloud-native-associate--kcna-)
  * [Certified Kubernetes Administrator (CKA)](#certified-kubernetes-administrator--cka-)
  * [Certified Kubernetes Application Developer (CKAD)](#certified-kubernetes-application-developer--ckad-)
- [Docker](#docker)
  * [Certified Associate](#certified-associate)
- [Redis](#redis)
  * [Certified Developer](#certified-developer)
- [MongoDB](#mongodb)
  * [Developer](#developer)
  * [Database Administrator](#database-administrator)
- [Cassandra](#cassandra)
  * [Garis Besar](#garis-besar-4)
- [Java](#java)
  * [Oracle Certified Associate](#oracle-certified-associate)
  * [Java SE 11 Developer](#java-se-11-developer)
- [Spring](#spring)
  * [Tutorial Spring](#tutorial-spring)
  * [Course Persiapan](#course-persiapan)
- [Python](#python)
  * [Garis Besar](#garis-besar-5)
- [Ruby](#ruby)
  * [Garis Besar](#garis-besar-6)
- [Certified Information Systems Security Professional](#certified-information-systems-security-professional)
- [Symfony](#symfony)
- [Pelengkap](#pelengkap)

<small><i><a href='http://ecotrust-canada.github.io/markdown-toc/'>Table of contents generated with markdown-toc</a></i></small>


## Petunjuk Umum

- [10 Best IT Certifications for Java Developers to Aim in 2021](https://medium.com/javarevisited/10-best-it-certifications-for-java-developers-5b4a78e3605d)

## Amazon Web Service

### Garis Besar

![](https://www.whizlabs.com/blog/wp-content/uploads/2021/01/AWS-Certifications-path.jpg)
![](https://acg-wordpress-content-production.s3.us-west-2.amazonaws.com/app/uploads/2021/03/AWS-Certification-Guide-A-Cloud-Guru-2021.png)

### Certified Cloud Practitioner

- [AWS Certified Cloud Practitioner Certification Course (CLF-C01) - Pass the Exam!](https://youtu.be/SOTamWNgDKc)

### Certified Cloud Developer Associate

- [AWS Certified Developer - Associate 2020 (PASS THE EXAM!)](https://youtu.be/RrKRN9zRBWs)
- [AWS Developer Certification | AWS Certified Developer Associate (FIRST 6 HOURS)](https://youtu.be/WYPG6Sdx1os)
- [AWS Certified Developer Associate for Beginners - Full Course](https://youtu.be/eDiWobhP1Ig)

### Certified Solution Architect Associate

- [AWS Certified Solutions Architect - Associate 2020 (PASS THE EXAM!)](https://youtu.be/Ia-UEYYR44s)

### Certified SysOps Administrator Associate

- [AWS SysOps Administrator Associate 2020 (PASS THE EXAM!)](https://youtu.be/KX_AfyrhlgQ)

## Google Cloud Platform

### Garis Besar

![](https://www.whizlabs.com/blog/wp-content/uploads/2018/09/Google-Cloud-Certifcations-Infographics.png)
![](https://blogs.sap.com/wp-content/uploads/2021/09/certifications.png)

- [Google Cloud Platform Certification resources.](https://github.com/sathishvj/awesome-gcp-certifications)

### Cloud Digital Leader

- [Google Cloud Digital Leader Certification Course - Pass the Exam!](https://youtu.be/UGRDM86MBIQ)

## Microsoft Azure

### Garis Besar

![](https://www.whizlabs.com/blog/wp-content/uploads/2021/11/new-role-based-microsoft-azure-certification-path.png)
![](https://www.whizlabs.com/blog/wp-content/uploads/2021/01/Microsoft-Azure-Exam.png)

### Azure Fundamentals

- [Microsoft Azure Fundamentals Certification Course (AZ-900) - Pass the exam in 3 hours!](https://youtu.be/NKEFWyqJ5XA)
- [Azure [AZ-900] Microsoft Azure Fundamentals training Full Course Online Study Guide (Updated 2021)](https://youtu.be/2fWEGpx-PB8)

### Azure Administrator

- [Azure Administrator Certification (AZ-104) - Full Course to PASS the Exam](https://youtu.be/10PbGbTUSAg)

### Azure Infrastructure Solutions

- [AZ-305 Design Azure Infrastructure Study Playlist](https://www.youtube.com/playlist?list=PLlVtbbG169nHSnaP4ae33yQUI3zcmP5nP)

## Oracle Cloud

### Garis Besar

![](https://www.oracle.com/a/ocom/img/cb112-cloud-training-diagram.jpg)

### Fundamentals Associate

- [The Oracle Foundations Associate Cloud Certification (PASS THE EXAM) – Full Course](https://youtu.be/si9tjcnxruU)
- [My Experience with Oracle Cloud Infrastructure Foundations Certification 1Z0-1085-20](https://www.linkedin.com/pulse/my-experience-oracle-cloud-infrastructure-foundations-birzu?trk=public_profile_article_view)

## Terraform

- [HashiCorp Terraform Associate Certification Course - Pass the Exam!](https://youtu.be/V4waklkBC38)

## Kubernetes

### Kubernetes and Cloud Native Associate (KCNA)

- [CNCF Kubernetes and Cloud Native Associate Certification Course (KCNA) - Pass the Exam!](https://youtu.be/AplluksKvzI)

### Certified Kubernetes Administrator (CKA)

- [Kubernetes Certified Administration](https://github.com/walidshaari/Kubernetes-Certified-Administrator)

### Certified Kubernetes Application Developer (CKAD)

- [CKAD Exercises](https://github.com/dgkanatsios/CKAD-exercises)

## Docker

### Certified Associate

- [Docker Certified Associate Exam Preparation Guide (v1.5 October 2020)](https://github.com/Evalle/DCA)

## Redis

### Certified Developer

- [Become a Redis Certified Developer: Practice Exams](https://www.udemy.com/course/redis-certified-developer-practice-exams/)

## MongoDB

### Developer

- [Certified MongoDB Developer - C100DEV Practice Tests - 2022](https://www.udemy.com/course/certified-mongodb-developer-exam-c100dev-practice-tests/)

### Database Administrator

- [Certified MongoDB - DBA Associate Practice Exams](https://www.udemy.com/course/certified-mongodb-dba-associate-practice-exams/)

## Cassandra

### Garis Besar

- [Why should you become a DataStax Certified professional?](https://www.datastax.com/dev/certifications)
- [Cassandra Certification Preparation](https://www.youtube.com/watch?v=ZA-NBczakqk)

## Java

### Oracle Certified Associate

- [Oracle Java Certification - Pass the Associate 1Z0-808 Exam.](https://www.udemy.com/course/oracle-java-associate-certification-exam-course-1z0-808/)
- [Java Certification : OCA (1Z0-808) Exam Simulation [2022]](https://www.udemy.com/course/java-oca/)

### Java SE 11 Developer

- [Java SE 11 Developer 1Z0-819 OCP Course - Part 1](https://www.udemy.com/course/java-se-11-developer-1z0-819-ocp-course-part-1/)
- [Java SE 11 Developer 1Z0-819 OCP Course - Part 2](https://www.udemy.com/course/java-se-11-developer-1z0-819-ocp-course-part-2/)

## Spring

### Tutorial Spring

- [Spring & Hibernate for Beginners (includes Spring Boot)](https://www.udemy.com/course/spring-hibernate-tutorial/)
- [Spring Framework 5: Beginner to Guru](https://www.udemy.com/course/spring-framework-5-beginner-to-guru/)

### Course Persiapan

- [Spring Professional Certification Exam Tutorial - Module 01](https://www.udemy.com/course/spring-certified-tutorial/)
- [Spring Professional Certification Exam Tutorial - Module 02](https://www.udemy.com/course/spring-professional-certification-exam-tutorial-module-02/)
- [Spring Professional Certification Exam Tutorial - Module 03](https://www.udemy.com/course/spring-professional-certification-exam-tutorial-module-03/)
- [Spring Professional Certification Exam Tutorial - Module 04](https://www.udemy.com/course/spring-professional-certification-exam-tutorial-module-04-spring-boot/)
- [Spring Professional Certification Exam Tutorial - Module 05](https://www.udemy.com/course/spring-professional-certification-exam-tutorial-module-05/)
- [Spring Professional Certification Exam Tutorial - Module 06](https://www.udemy.com/course/spring-professional-certification-exam-tutorial-module-06/)
- [Spring Professional Certification Exam Tutorial - Module 07](https://www.udemy.com/course/spring-professional-certification-exam-tutorial-module-07/)
- [Spring Professional Certification Exam Tutorial - Module 08](https://www.udemy.com/course/spring-professional-certification-exam-tutorial-module-08/)

## Python

### Garis Besar

- [Top 5 Python Certification for 2022](https://hackr.io/blog/python-certification)

## Ruby

### Garis Besar

- [Ruby Association Certified Ruby Programmer Examination](https://www.ruby.or.jp/en/certification/examination/)

## Certified Information Systems Security Professional

- [CISSP Certification Course – PASS the Certified Information Security Professional Exam!](https://youtu.be/M1_v5HBVHWo)

## Symfony

- [Symfony Certification Preparation List](https://github.com/ThomasBerends/symfony-certification-preparation-list)

## Pelengkap

- [PanXProject/awesome-certificates: List of free dev courses with certificates & badges.](https://github.com/PanXProject/awesome-certificates)
